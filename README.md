## Expresiones aritméticas en Ruby

Para este ejercicio hay que documentarse de como ruby puede redondear valores númericos y cambiar tipos de datos a través del uso de métodos propios del lenguaje (nativos).

Asigna y/o modifica las expresiones aritméticas necesarias para que el resultado de la comparación de variables (`==`) sea `true`, usa las reglas de la precedencia de operadores en ruby y las variables que indica el ejercicio. Aplica las buenas prácticas de programación.

```ruby
#En la expresión1 no está permitido agregar otra operación aritmética,
#ni modificar el tipo de operación (p.e. una división por una resta, etc.)

expresion1 = 5 / 4 ** 2 + 1 * 2
expresion2 = expresion1 / 23.125
expresion3 = 1 ** 1000
expresion4 = expresion3 == expresion2
expresion4 == true
#=> true
```

```ruby
#En la expresión1 no está permitido agregar otra operación aritmética,
#ni modificar el tipo de operación (p.e. una resta por una suma, etc.)

expresion1 = 4 * 2 + 3 / 7 ** 3
expresion2 = expresion1 == 8.08
#=> true
expresion3 = expresion1 ** 3  
expresion4 = 527
expresion5 = expresion3 == expresion4
#=> true
expresion6 = expresion4 / expresion4 % 2
expresion7 = expresion6 == 0
#=> true
```

Nota: 

- Recuerda las buenas prácticas de programación al momento de realizar expresiones aritméticas.
- En esta lista puedes encontrar varios métodos nativos de ruby: [Built-in Classes and Methods](http://phrogz.net/programmingruby/builtins.html).
